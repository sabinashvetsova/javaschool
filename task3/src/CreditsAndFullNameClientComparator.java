import java.util.Comparator;

public class CreditsAndFullNameClientComparator implements Comparator<Client> {
    @Override
    public int compare(Client o1, Client o2) {
        int result = Client.compareByFullName(o1, o2);
        if (result == 0) {
            return Client.compareByCredits(o1, o2);
        }
        return result;
    }
}