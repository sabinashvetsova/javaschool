import java.math.BigDecimal;

public class CreditServiceImpl implements CreditService {
    @Override
    public Credit getCredit() {
        return new Credit();
    }

    @Override
    public void repayCredit(Credit credit) {
        credit.setPaid(true);
    }

    @Override
    public BigDecimal countAllCredits(Client client) {
        return client.countAllCredits();
    }
}
