import java.math.BigDecimal;
import java.util.Objects;

public class Credit {
    private long creditId;
    private boolean isPaid;
    private BigDecimal amount;

    public Credit(int creditId, boolean isPaid, BigDecimal amount) {
        this.creditId = creditId;
        this.isPaid = isPaid;
        this.amount = amount;
    }

    public Credit() {
    }

    public void setPaid(boolean paid) {
        isPaid = paid;
    }

    @Override
    public String toString() {
        return "<Credit>\n" +
                "  <creditId>" + creditId + "</creditId>\n" +
                "  <isPaid>" + isPaid + "</isPaid>\n" +
                "  <amount>" + amount + "</amount>\n" +
                "</Credit>";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Credit credit = (Credit) o;
        return creditId == credit.creditId && isPaid == credit.isPaid && Objects.equals(amount, credit.amount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(creditId, isPaid, amount);
    }

    public BigDecimal getAmount() {
        return amount;
    }
}
