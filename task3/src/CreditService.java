import java.math.BigDecimal;

public interface CreditService {
    public Credit getCredit();

    public void repayCredit(Credit credit);

    public BigDecimal countAllCredits(Client client);
}
