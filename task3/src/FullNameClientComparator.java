import java.util.Comparator;

public class FullNameClientComparator implements Comparator<Client> {

    @Override
    public int compare(Client o1, Client o2) {
        return Client.compareByFullName(o1, o2);
    }
}