import java.util.Comparator;

public class CreditsClientComparator implements Comparator<Client> {
    @Override
    public int compare(Client o1, Client o2) {
        return Client.compareByCredits(o1, o2);
    }
}