import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Objects;

public class Client {
    private long clientId;
    private String name;
    private String surname;
    private String patronymic;
    private ArrayList<Credit> credits = new ArrayList<>();

    public Client(int clientId, String name, String surname, String patronymic) {
        this.clientId = clientId;
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client client = (Client) o;
        return clientId == client.clientId && Objects.equals(name, client.name) && Objects.equals(surname, client.surname) && Objects.equals(patronymic, client.patronymic);
    }

    @Override
    public int hashCode() {
        return Objects.hash(clientId, name, surname, patronymic);
    }

    @Override
    public String toString() {
        return "{" + "\n" +
                "   \"clientId\": " + clientId + ",\n" +
                "   \"name\": \"" + name + "\",\n" +
                "   \"surname\": \"" + surname + "\",\n" +
                "   \"patronymic\": \"" + patronymic + "\"\n" +
                "}";
    }

    public BigDecimal countAllCredits() {
        BigDecimal sum = BigDecimal.valueOf(0);
        for (Credit credit : credits) {
            sum = sum.add(credit.getAmount());
        }
        return sum;
    }

    public void addCredit(Credit credit) {
        this.credits.add(credit);
    }

    public String getFullName() {
        return surname + " " + name + " " + patronymic;
    }

    public static int compareByFullName(Client o1, Client o2) {
        return o1.getFullName().compareTo(o2.getFullName());
    }

    public static int compareByCredits(Client o1, Client o2) {
        BigDecimal result = o1.countAllCredits().subtract(o2.countAllCredits());
        BigDecimal zero = BigDecimal.valueOf(0);
        return Integer.compare(result.compareTo(zero), 0);
    }
}
