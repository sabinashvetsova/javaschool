public interface CreditService {
    public Credit getCredit();

    public void repayCredit(Credit credit);
}
