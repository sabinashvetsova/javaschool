import java.util.Objects;

public class Client {
    private long clientId;
    private String name;
    private String surname;
    private String patronymic;

    public Client(int clientId, String name, String surname, String patronymic) {
        this.clientId = clientId;
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client client = (Client) o;
        return clientId == client.clientId && Objects.equals(name, client.name) && Objects.equals(surname, client.surname) && Objects.equals(patronymic, client.patronymic);
    }

    @Override
    public int hashCode() {
        return Objects.hash(clientId, name, surname, patronymic);
    }

    @Override
    public String toString() {
        return "{" + "\n" +
                "   \"clientId\": " + clientId + ",\n" +
                "   \"name\": \"" + name + "\",\n" +
                "   \"surname\": \"" + surname + "\",\n" +
                "   \"patronymic\": \"" + patronymic + "\"\n" +
                "}";
    }
}
