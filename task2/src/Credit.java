import java.util.Objects;

public class Credit {
    private long creditId;
    private boolean isPaid;

    public Credit(int creditId, boolean isPaid) {
        this.creditId = creditId;
        this.isPaid = isPaid;
    }

    public Credit() {
    }

    public void setPaid(boolean paid) {
        isPaid = paid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Credit credit = (Credit) o;
        return creditId == credit.creditId && isPaid == credit.isPaid;
    }

    @Override
    public int hashCode() {
        return Objects.hash(creditId, isPaid);
    }

    @Override
    public String toString() {
        return "<Credit>\n" +
                "  <creditId>" + creditId + "</creditId>\n" +
                "  <isPaid>" + isPaid + "</isPaid>\n" +
                "</Credit>";
    }
}
