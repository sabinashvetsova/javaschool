public class CreditServiceImpl implements CreditService {
    @Override
    public Credit getCredit() {
        return new Credit();
    }

    @Override
    public void repayCredit(Credit credit) {
        credit.setPaid(true);
    }
}
