import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

public class Transfer {
    private long transferId;
    private Date transferDate;
    private BigDecimal amount;
    private String currency;
    private Client sender;
    private Client recipient;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transfer transfer = (Transfer) o;
        return transferId == transfer.transferId && Objects.equals(transferDate, transfer.transferDate) && Objects.equals(amount, transfer.amount) && Objects.equals(currency, transfer.currency) && Objects.equals(sender, transfer.sender) && Objects.equals(recipient, transfer.recipient);
    }

    @Override
    public int hashCode() {
        return Objects.hash(transferId, transferDate, amount, currency, sender, recipient);
    }

    @Override
    public String toString() {
        return "Transfer{" +
                "transferId=" + transferId +
                ", transferDate=" + transferDate +
                ", amount=" + amount +
                ", currency='" + currency + '\'' +
                ", sender=" + sender +
                ", recipient=" + recipient +
                '}';
    }
}
